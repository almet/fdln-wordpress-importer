#!/usr/bin/env python

from setuptools import setup

requires = ['requests', 'arrow', 'progressist', 'tablib', 'python-wordpress-xmlrpc']

entry_points = {
    'console_scripts': [
        'fdln-importer = fdln_importer:main',
    ]
}

setup(
    name="fdln-importer",
    version="0.0.1",
    author='Alexis Metaireau',
    author_email='alexis@notmyidea.org',
    description="Import datasheet into wordpress events. ",
    packages=['fdln_importer', ],
    include_package_data=True,
    install_requires=requires,
    entry_points=entry_points,
)
