# Importer les évènements dans le site wordpress

Voici donc, un petit script pour se faciliter la vie en tant qu'organisateurices du festival des libertés numériques. L'outil utilise un export de données (au format xlsx), et créé les catégories qui vont bien sur le wordpress, et créé des articles pour chaque évènement, en spécifiant les dates, ainsi que la localisation si celui ci est précisé.

## Installation

Alors pour commencer, il faut l'installer. Il faut avoir python 3 d'installé sur sa machine. Ensuite, dans un terminal :

```bash

git clone https://framagit.org/almet/fdln-wordpress-importer.git
cd fdln-wordpress-importer

pip install -e .
```

## Utilisation

Ensuite pour l'utiliser c'est une ligne de commande « classique », voici un exemple :

```bash
fdln-importer --dataset="/path/to/prog-fdln.xlsx" --server=https://fdln2020test.home.blog --username=yourusername --password=yourpassword --start-date=2020-02-01 --end-date=2020-02-16 --dry-run

✔ Connecté au serveur wordpress en tant que alexismetaireau2
✔ Lecture de 103 entrées depuis le fichier /home/dan/dev/wordpress-importer/prog-fdln.xlsx.
✔ Des catégories pour les dates existent déjà.
Géolocalisation ████████████████████████████████████████████████████████████████████████████████████████ 100.00% (103/103)
Publication ████████████████████████████████████████████████████████████████████████████████████████████ 100.00% (103/103)
✔ Et voilà !
```

Une fois que l'import a fonctionné sans encombres en mode `--dry-run`, vous pouvez enlever l'option `--dry-run` et la publication devrait se faire sur le site wordpress.