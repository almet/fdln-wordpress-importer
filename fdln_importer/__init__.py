# -*- coding: utf-8 -*-
import argparse
import locale
from datetime import datetime, timedelta

import arrow
import requests

from progressist import ProgressBar
from tablib import Dataset

from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods.users import GetUserInfo
from wordpress_xmlrpc.methods.posts import NewPost
from wordpress_xmlrpc.methods import taxonomies


locale.setlocale(locale.LC_ALL, "fr_FR.UTF-8")

ADDOK_URL = "http://api-adresse.data.gouv.fr/search/"
CONTENT_TEMPLATE = """{description}

Organisé par {organisation}
Lieu : {location}
Date : {date_display}
Début : {event_begin_display}
Fin : {event_end_display}
Accessible aux personnes à mobilité réduite ? {pmr}
"""


def noop(*args, **kws):
    return None


def shout(e):
    print(e)


class Importer:
    def __init__(self, server, username, password, dry_run=False, debug=False):
        self.debug = shout if debug else noop
        self.warning = shout if debug else noop
        self.info = shout
        self.init_wordpress_client(server, username, password)
        self.dry_run = dry_run

    def run(self, dataset, start_date, end_date):
        records = self.read_dataset(dataset)

        prepared_data = list(map(self.build_post, records.dict))

        self.create_dates_categories(start_date, end_date)

        geolocalized_data = []
        total = len(prepared_data)
        geoloc_bar = ProgressBar(total=total, done_char="█", prefix="Géolocalisation")
        for record in geoloc_bar.iter(prepared_data):
            geolocalized_data.append(self.geolocalize(record))

        categories = set()
        for data in geolocalized_data:
            categories.update(set(data["categories"]))

        bar = ProgressBar(total=total, done_char="█", prefix="Publication")
        for record in bar.iter(geolocalized_data):
            self.upload_post(record)

        self.info("✔ Et voilà !")

    def read_dataset(self, filename):
        records = Dataset().load(open(filename, "rb").read())
        records.headers = (
            "name",
            "category",
            "description",
            "extra_category",
            "_",
            "location",
            "pmr",
            "date",
            "start_time",
            "end_time",
            "organisation_type",
            "organisation",
            "tarification",
            "price",
            "contact",
            "___",
            "____",
            "_____",
            "______",
            "_______",
        )
        self.info(f"✔ Lecture de {len(records)} entrées depuis le fichier {filename}.")
        return records

    def geolocalize(self, data):
        if "location" in data:
            response = requests.get(
                ADDOK_URL, params={"q": data["location"], "limit": 5}
            )
            if "features" in response.json():
                locations = response.json()["features"]
                if locations:
                    data["long"], data["lat"] = locations[0]["geometry"]["coordinates"]
            return data

    def init_wordpress_client(self, server, username, password):
        self._client = Client(f"{server}/xmlrpc.php", username, password)
        username = self._client.call(GetUserInfo())
        self.info(f"✔ Connecté au serveur wordpress en tant que {username}")

    def create_dates_categories(self, start_date, end_date):
        self._categories = self._client.call(taxonomies.GetTerms("category"))
        dates = [
            start_date + timedelta(days=x)
            for x in range((end_date - start_date).days + 1)
        ]
        for date in dates:
            try:
                term = date.strftime("%d %B")
                self.create_category(term)
            except Exception as e:
                self.info(f"✔ Une catégorie pour la date '{term}' existe déjà. {e}")
            else:
                self.info(f"✔ Catégorie pour la date '{term}' créée.")
        self._categories = self._client.call(taxonomies.GetTerms("category"))

    def create_category(self, category):
        if any([c for c in self._categories if c.name == category]):
            return
        term = taxonomies.WordPressTerm()
        term.taxonomy = "category"
        term.name = category
        self._client.call(taxonomies.NewTerm(term))

    def build_post(self, record):
        title = record["name"]
        categories = []

        if record["category"] and "," in record["category"]:
            categories = [c.strip() for c in record["category"].split(",")]

        elif record["category"]:
            category = record["category"]
            categories.append(category)
            title = f"{category} : {title}"

        if record["extra_category"] in ("Partage", "partage"):
            categories.append("Partage")

        date = self._convert_date(record["date"])
        event_begin, event_end = self._get_dates_from_record(record, date)

        data = {
            "title": title,
            "categories": categories,
            "event_begin": event_begin,
            "event_end": event_end,
            "event_begin_display": event_begin.format(
                "dddd DD MMM HH:mm", locale="fr_FR"
            )
            if hasattr(event_begin, "format")
            else event_begin,
            "event_end_display": event_end.format("dddd DD MMM HH:mm", locale="fr_FR")
            if hasattr(event_end, "format")
            else event_end,
            "location": record["location"],
            "description": record["description"],
            "date": date,
            "date_display": date.format("dddd DD MMM", locale="fr_FR")
            if hasattr(date, "format")
            else date,
            "organisation": record["organisation"],
            "pmr": record["pmr"] or "Non communiqué",
        }

        data["content"] = CONTENT_TEMPLATE.format(**data)
        return data

    def upload_post(self, data):
        post = WordPressPost()
        post.title = data["title"]
        post.content = data["content"]

        if hasattr(data["date"], "strftime"):
            term = data["date"].strftime("%d %B")
            categories = [
                c for c in self._categories if c.name == term or c.name == term[1:-1]
            ]
            post.terms = categories

        for cat in data["categories"]:
            post.terms.extend([c for c in self._categories if c.name == cat])
            try:
                self.create_category(cat)
            except:
                pass

        post.custom_fields = []

        custom_fields = {}

        if data["event_begin"]:
            custom_fields["event_begin"] = data["event_begin"].isoformat()

        if data["event_end"]:
            custom_fields["event_end"] = data["event_end"].isoformat()

        if "location" in data:
            custom_fields["geo_address"] = data["location"]

        if "lat" in data:
            custom_fields["geo_latitude"] = data["lat"]
            custom_fields["geo_longitude"] = data["long"]

        for key, value in custom_fields.items():
            post.custom_fields.append({"key": key, "value": value})

        try:
            self.debug(post.title)
            self.debug(post.content)
            self.debug(post.custom_fields)
            if not self.dry_run:
                self._client.call(NewPost(post))
        except Exception as e:
            self.warning(e)

    def _convert_date(self, date):
        if type(date) == datetime:
            return date
        if type(date) == int:
            return arrow.get(
                datetime.fromordinal(datetime(1900, 1, 1).toordinal() + date)
            )
        else:
            if type(date) == str and "00:00:00" in date:
                date = date.replace("00:00:00", "")
            try:
                date = self._get_date_from_string(
                    date,
                    (
                        "YYYY-MM-DD [00:00:00]",
                        "D MMMM",
                        "Do MMMM",
                        "D MMMM YYYY",
                        "Do MMMM YYYY",
                    ),
                    "fr_FR",
                )
                date = date.replace(year=2020)
                return date
            except:
                return None

    def _get_date_from_string(self, time_str, formats, locale="en_US"):
        for fmt in formats:
            try:
                return arrow.get(time_str, fmt, locale=locale)
            except ValueError as e:
                self.debug(e)
                pass
        raise ValueError(f"Unable to parse {time_str}")

    def _get_dates_from_record(self, record, date):
        # We have 3 fields : date, start_time and end_time.
        # From this, we want to get two datetimes for start and end.
        if not date:
            self.warning(
                f"✖ Impossible de determiner l'heure sans avoir la date ({date}). Next"
            )
            return None, None

        def _get_date_from_time(time_str):
            try:
                time = self._get_date_from_string(
                    time_str,
                    (
                        "H:mm A",
                        "HH:mm A",
                        "H:mmA",
                        "HH:mmA",
                        "HH[h]",
                        "HH:mm",
                        "HH[h]mm",
                        "HH[h]mm A",
                    ),
                )
                # time = datetime.strptime(time_str, "%H:%M %p")
            except Exception as e:
                self.warning(f"✖ Impossible d'utiliser l'heure {time_str}")
                return None

            new_date = time.replace(year=date.year, month=date.month, day=date.day)
            return new_date

        event_begin = _get_date_from_time(record["start_time"])
        event_end = _get_date_from_time(record["end_time"])
        return event_begin, event_end


def valid_date(s):
    try:
        return datetime.strptime(s, "%Y-%m-%d")
    except ValueError:
        msg = "Diantre, {0} n'est pas une date valide. '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def main():
    parser = argparse.ArgumentParser(
        description="Un outil qui permet d'importer plus facilement les évènements pour le FDLN"
    )

    parser.add_argument(
        "-d",
        "--dataset",
        dest="dataset",
        help="Location of the dataset file.",
        default="dataset.json",
    )

    parser.add_argument(
        "--server",
        dest="server",
        help="Wordpress server to connect to.",
        default="http://localhost",
    )

    parser.add_argument("--username", dest="username", default=None, help="Username.")

    parser.add_argument(
        "-p", "--password", dest="password", default=None, help="Password."
    )

    parser.add_argument(
        "--dry-run",
        dest="dry_run",
        action="store_true",
        default=False,
        help="Simulate.",
    )

    parser.add_argument(
        "--debug", dest="debug", action="store_true", default=False, help="Debug.",
    )

    parser.add_argument(
        "--start-date",
        dest="start_date",
        help="Date de début du festival - YYYY-MM-DD",
        required=True,
        type=valid_date,
    )

    parser.add_argument(
        "--end-date",
        dest="end_date",
        help="Date de fin du festival - YYYY-MM-DD",
        required=True,
        type=valid_date,
    )

    args = parser.parse_args()
    importer = Importer(
        args.server, args.username, args.password, args.dry_run, args.debug
    )
    importer.run(args.dataset, args.start_date, args.end_date)
